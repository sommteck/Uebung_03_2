namespace Uebung_03_2
{
    partial class Kalkulator
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_operand1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_operand2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_ergebnis = new System.Windows.Forms.TextBox();
            this.cmd_plus = new System.Windows.Forms.Button();
            this.cmd_minus = new System.Windows.Forms.Button();
            this.cmd_multiplicate = new System.Windows.Forms.Button();
            this.cmd_divide = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operand 1";
            // 
            // txt_operand1
            // 
            this.txt_operand1.Location = new System.Drawing.Point(27, 56);
            this.txt_operand1.Name = "txt_operand1";
            this.txt_operand1.Size = new System.Drawing.Size(197, 22);
            this.txt_operand1.TabIndex = 1;
            this.txt_operand1.Text = "0";
            this.txt_operand1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Operand 2";
            // 
            // txt_operand2
            // 
            this.txt_operand2.Location = new System.Drawing.Point(27, 150);
            this.txt_operand2.Name = "txt_operand2";
            this.txt_operand2.Size = new System.Drawing.Size(197, 22);
            this.txt_operand2.TabIndex = 2;
            this.txt_operand2.Text = "0";
            this.txt_operand2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ergebnis";
            // 
            // txt_ergebnis
            // 
            this.txt_ergebnis.Location = new System.Drawing.Point(27, 230);
            this.txt_ergebnis.Multiline = true;
            this.txt_ergebnis.Name = "txt_ergebnis";
            this.txt_ergebnis.ReadOnly = true;
            this.txt_ergebnis.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_ergebnis.Size = new System.Drawing.Size(298, 63);
            this.txt_ergebnis.TabIndex = 5;
            this.txt_ergebnis.TabStop = false;
            this.txt_ergebnis.Text = "0";
            this.txt_ergebnis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cmd_plus
            // 
            this.cmd_plus.Location = new System.Drawing.Point(374, 37);
            this.cmd_plus.Name = "cmd_plus";
            this.cmd_plus.Size = new System.Drawing.Size(44, 33);
            this.cmd_plus.TabIndex = 3;
            this.cmd_plus.Text = " + ";
            this.cmd_plus.UseVisualStyleBackColor = true;
            this.cmd_plus.Click += new System.EventHandler(this.cmd_plus_Click);
            // 
            // cmd_minus
            // 
            this.cmd_minus.Location = new System.Drawing.Point(435, 37);
            this.cmd_minus.Name = "cmd_minus";
            this.cmd_minus.Size = new System.Drawing.Size(44, 33);
            this.cmd_minus.TabIndex = 4;
            this.cmd_minus.Text = " - ";
            this.cmd_minus.UseVisualStyleBackColor = true;
            this.cmd_minus.Click += new System.EventHandler(this.cmd_minus_Click);
            // 
            // cmd_multiplicate
            // 
            this.cmd_multiplicate.Location = new System.Drawing.Point(374, 79);
            this.cmd_multiplicate.Name = "cmd_multiplicate";
            this.cmd_multiplicate.Size = new System.Drawing.Size(44, 36);
            this.cmd_multiplicate.TabIndex = 5;
            this.cmd_multiplicate.Text = " x ";
            this.cmd_multiplicate.UseVisualStyleBackColor = true;
            this.cmd_multiplicate.Click += new System.EventHandler(this.cmd_multiplicate_Click);
            // 
            // cmd_divide
            // 
            this.cmd_divide.Location = new System.Drawing.Point(435, 79);
            this.cmd_divide.Name = "cmd_divide";
            this.cmd_divide.Size = new System.Drawing.Size(44, 36);
            this.cmd_divide.TabIndex = 6;
            this.cmd_divide.Text = " / ";
            this.cmd_divide.UseVisualStyleBackColor = true;
            this.cmd_divide.Click += new System.EventHandler(this.cmd_divide_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(374, 131);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(105, 36);
            this.cmd_clear.TabIndex = 10;
            this.cmd_clear.TabStop = false;
            this.cmd_clear.Text = " C ";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(374, 262);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(105, 31);
            this.cmd_end.TabIndex = 11;
            this.cmd_end.TabStop = false;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 327);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_divide);
            this.Controls.Add(this.cmd_multiplicate);
            this.Controls.Add(this.cmd_minus);
            this.Controls.Add(this.cmd_plus);
            this.Controls.Add(this.txt_ergebnis);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_operand2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_operand1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Kalkulator";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_operand1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_operand2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_ergebnis;
        private System.Windows.Forms.Button cmd_plus;
        private System.Windows.Forms.Button cmd_minus;
        private System.Windows.Forms.Button cmd_multiplicate;
        private System.Windows.Forms.Button cmd_divide;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
    }
}