using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uebung_03_2
{
    public partial class Kalkulator : Form
    {
        double operand_1, operand_2, ergebnis;

        public Kalkulator()
        {
            InitializeComponent();

            ToolTip help = new ToolTip();
            help.SetToolTip(cmd_clear, "Ein- und Ausgaben löschen");
            help.SetToolTip(cmd_end, "Programm beenden");
            help.SetToolTip(cmd_plus, "Addieren");
            help.SetToolTip(cmd_minus, "Subtrahieren");
            help.SetToolTip(cmd_multiplicate, "Multiplizieren");
            help.SetToolTip(cmd_divide, "Dividieren");
        }

        private void Abfangen()
        {
            MessageBox.Show("Bitte nur Ziffern eingeben!");
            txt_operand1.Text = txt_operand2.Text = "0";
            txt_operand1.Focus();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_ergebnis.Text = txt_operand1.Text = txt_operand2.Text = "0";
            txt_operand1.Focus();
        }

        private void cmd_plus_Click(object sender, EventArgs e)
        {
            try
            {
                operand_1 = Convert.ToDouble(txt_operand1.Text);
                operand_2 = Convert.ToDouble(txt_operand2.Text);
                ergebnis = operand_1 + operand_2;
                txt_ergebnis.Text = ergebnis.ToString();
            }

            catch
            {
                Abfangen();
            }
        }

        private void cmd_minus_Click(object sender, EventArgs e)
        {
            try
            {
                operand_1 = Convert.ToDouble(txt_operand1.Text);
                operand_2 = Convert.ToDouble(txt_operand2.Text);
                ergebnis = operand_1 - operand_2;
                txt_ergebnis.Text = ergebnis.ToString();
            }

            // Erst werden die expliziten Fehler abgehandelt
            catch (FormatException err)
            {
                txt_ergebnis.Text = Convert.ToString(err);
                MessageBox.Show("Bitte nur Ziffern als Operanten eingeben!");
            }

            catch (OverflowException err)
            {
                txt_ergebnis.Text = Convert.ToString(err);
                MessageBox.Show("Operanten außerhalb des Bereich!");
            }

            // Danach werden die allgemeinen Fehler abgehandelt
            catch
            {
                Abfangen();
            }
        }

        private void cmd_multiplicate_Click(object sender, EventArgs e)
        {
            try
            {
                operand_1 = Convert.ToDouble(txt_operand1.Text);
                operand_2 = Convert.ToDouble(txt_operand2.Text);
                ergebnis = operand_1 * operand_2;
                txt_ergebnis.Text = ergebnis.ToString();
            }

            catch
            {
                Abfangen();
            }
        }

        private void cmd_divide_Click(object sender, EventArgs e)
        {
            try
            {
                operand_1 = Convert.ToDouble(txt_operand1.Text);
                operand_2 = Convert.ToDouble(txt_operand2.Text);
                if (txt_operand2.Text != "0")
                {
                    ergebnis = operand_1 / operand_2;
                    txt_ergebnis.Text = ergebnis.ToString();

                }
                else
                {
                    MessageBox.Show("Division durch 0 ist nicht erlaubt!");
                    txt_operand1.Text = txt_operand2.Text = txt_ergebnis.Text = "0";
                    txt_operand2.Focus();
                }
            }

            catch
            {
                Abfangen();
            }
        }
    }
}